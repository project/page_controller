<?php
/**
 * @file
 * Example of hook_menu_usage().
 */

/**
 * Implements hook_menu().
 */
function hook_menu() {

  $menu = array();
  $menu['my/page/callback'] = array(

    // Your page callback should always be the same.
    'page callback' => '\Drupal\\page_controller\\Controller\\PageController::createPage',

    // Your page arguments should start with your Controller, and the method
    // to call.
    'page arguments' => array(
      '\Drupal\\my_module\\Pages\\ExampleController',
      'myPageControllerViewCallback',
    ),
  );

  return $menu;
}
