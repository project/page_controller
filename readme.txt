Page Controller
by xtfer

Copyright(c) 2013 Chris Skene
Licence: GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
Author: Chris Skene chris at xtfer dot com

Page Controller provides a standardised Controller class for object-oriented
page callbacks in Drupal 7, a bit like Drupal 8 page controllers.

------------------------------------------------------------
Usage
------------------------------------------------------------

Hook_menu accepts any string for the page callback. While passing a standard
object or an array suitable for use in call_user_func() won't work here, a
static object callback will. We leverage this to provide a standard controller
which can be reused for object oriented page callbacks.

<?php
/**
 * Implements hook_menu().
 */
function hook_menu() {

  $menu = array();
  $menu['my/page/callback'] = array(
    ...
    // Your page callback should always be the same.
    'page callback' => '\Drupal\\page_controller\\Controller\\PageController::createPage',

    // Your page arguments should start with your Controller, and the method
    // to call.
    'page arguments' => array(
      '\Drupal\\my_module\\Pages\\ExampleController',
      'myPageControllerViewCallback',
    ),
    ...
  );

  return $menu;
}
?>

then, create a new page controller at

<?php
/**
 * @file
 * Contains an example controller.
 */

namespace Drupal\my_module\Pages

use Drupal\page_controller\Controller\PageController;

/**
 * Class ExampleController.
 */
class ExampleController extends PageController {

  /**
   * View callback.
   */
  public function myPageControllerViewCallback($arg1, $arg2) {
    // Do something with $args

    return 'Some output';
  }
}
?>