<?php
/**
 * @file
 * Contains a PageController.
 *
 * @copyright Copyright(c) 2013 Chris Skene
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 */

namespace Drupal\page_controller\Controller;

/**
 * Class PageController
 *
 * @package Drupal\page_controller\Pages
 */
class PageController {

  /**
   * Static factory function.
   */
  static public function createPage() {

    $args = func_get_args();
    $controller_name = array_shift($args);
    $method = array_shift($args);

    if (class_exists($controller_name)) {
      $controller = new $controller_name();

      return call_user_func_array(array($controller, $method), $args);
    }

    throw new \Exception('Invalid Page Controller');
  }
}
