<?php
/**
 * @file
 * Contains an example controller.
 */

namespace Drupal\page_controller\Example;

use Drupal\page_controller\Controller\PageController;

/**
 * Class ExampleController
 *
 * @package Drupal\page_controller\Example
 */
class ExampleController extends PageController {

  /**
   * Example view callback.
   */
  public function myPageControllerViewCallback($arg1, $arg2) {
    // Do something with $args.
    return 'Some output';
  }
}
